package ru.t1.nikitushkina.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class ProjectGetByIndexResponse extends AbstractProjectResponse {

    public ProjectGetByIndexResponse(@Nullable ProjectDTO project) {
        super(project);
    }

}
