package ru.t1.nikitushkina.tm.api.repository.dto;

import ru.t1.nikitushkina.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {
}
