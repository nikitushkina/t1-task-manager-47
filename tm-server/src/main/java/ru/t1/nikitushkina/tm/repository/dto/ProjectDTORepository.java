package ru.t1.nikitushkina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.nikitushkina.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO>
        implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final ProjectDTO project = new ProjectDTO(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    protected Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

}
