package ru.t1.nikitushkina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.repository.model.IProjectRepository;
import ru.t1.nikitushkina.tm.model.Project;

import javax.persistence.EntityManager;

public class ProjectRepository extends AbstractUserOwnedRepository<Project>
        implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    protected Class<Project> getClazz() {
        return Project.class;
    }

}
